FROM ubuntu:20.04

EXPOSE 3838

ENV DEBIAN_FRONTEND=noninteractive
RUN ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && apt-get update \
    && apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade \
    && apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade \
#    && apt-get autoremove -y \
    && apt-get install --no-install-recommends -y \
        tzdata \
        wget

ENTRYPOINT [ "/usr/bin/shiny-server" ]

# see https://cran.r-project.org/bin/linux/ubuntu/
RUN apt-get install --no-install-recommends -y \
        dirmngr \
        software-properties-common \
    && wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc \
    && add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" \
    && apt-get install --no-install-recommends -y \
        r-base-dev

# see https://www.rstudio.com/products/shiny/download-server/ubuntu/
RUN R -e "install.packages('shiny', repos='https://cran.rstudio.com/')" \
    && apt-get install --no-install-recommends -y \
        gdebi-core \
    && wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.16.958-amd64.deb \
    && yes | gdebi shiny-server-*.deb \
    && rm -f shiny-server-*.deb

RUN apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# see https://docs.rstudio.com/shiny-server/#r-markdown
RUN R -e "install.packages('rmarkdown')"
