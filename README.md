# docker-shiny-server

Shiny Server in Docker

```
docker pull wrzlbrmft/shiny-server:latest
```

```
docker run -p 3838:3838 wrzlbrmft/shiny-server:latest
```

Next, open http://localhost:3838/ .

See also:

  * https://www.rstudio.com/products/shiny/shiny-server/
  * https://hub.docker.com/r/wrzlbrmft/shiny-server/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
